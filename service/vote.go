package service

import (
	"go-web-app-demo/common"
	"go-web-app-demo/dao/redis"
	"strconv"
)

type VoteReq struct {
	UserID    uint64
	PostID    int64 `json:"postId,string" binding:"required"`
	Direction int8  `json:"direction,string" binding:"oneof=-1 0 1"`
}

func PostVote(vr *VoteReq) *common.Result {

	pid := strconv.FormatInt(vr.PostID, 10)
	uid := strconv.FormatUint(vr.UserID, 10)

	if err := redis.PostVote(pid, uid, float64(vr.Direction)); err != nil {
		return common.NewFailed(err.Error())
	}
	return common.NewSucceed("投票成功")
}
