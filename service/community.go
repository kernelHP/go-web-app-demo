package service

import (
	"go-web-app-demo/common"
	"go-web-app-demo/dao/db"
)

func ListCommunity() *common.Result {

	list, err := db.ListCommunity()

	if err != nil {
		return common.NewFailed(err.Error())
	}

	return common.NewSucceed(list)
}

func GetCommunityByID(id uint64) *common.Result {

	c, err := db.GetCommunityByID(id)

	if err != nil {
		return common.NewFailed(err.Error())
	}

	return common.NewSucceed(c)
}
