package util

import (
	"errors"
	"github.com/gin-gonic/gin"
	"go-web-app-demo/controller/middleware"
)

func GetCurrentUser(c *gin.Context) (userID uint64, err error) {

	v, exists := c.Get(middleware.UserIDKey)

	if !exists {
		err = errors.New("用户未登录")
		return
	}

	userID, ok := v.(uint64)

	if !ok {
		return
	}

	return userID, err
}
