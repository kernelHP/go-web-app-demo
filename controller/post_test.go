package controller

import (
	"bytes"
	"github.com/gin-gonic/gin"
	"github.com/magiconair/properties/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestNewPost(t *testing.T) {

	gin.SetMode(gin.TestMode)
	r := gin.Default()
	url := "/post"
	r.POST(url, NewPost)

	body := `{
    "communityID": 1,
    "status": 2,
    "title": "嘎嘎嘎嘎大苏打",
    "content": "sdjaskdhkajshd等哈数据库的干哈司空见惯的啊速度0啊苏打水电话卡技术的咯技术的kk"
	}`

	req, _ := http.NewRequest(http.MethodPost, url, bytes.NewReader([]byte(body)))
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, http.StatusOK, w.Body.String())

}
