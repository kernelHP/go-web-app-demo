package controller

import (
	"github.com/gin-gonic/gin"
	"go-web-app-demo/router"
	"go-web-app-demo/service"
	"net/http"
	"strconv"
)

func init() {
	router.AddHandler(&router.Handler{
		Method:       "get",
		RelativePath: "/communities",
		HandleFunc:   ListCommunity,
		NeedAuth:     false,
	})

	router.AddHandler(&router.Handler{
		Method:       "get",
		RelativePath: "/community/:id",
		HandleFunc:   GetCommunity,
		NeedAuth:     false,
	})
}

func ListCommunity(c *gin.Context) {

	c.JSON(http.StatusOK, service.ListCommunity())
}

func GetCommunity(c *gin.Context) {

	id, err := strconv.ParseUint(c.Param("id"), 10, 64)

	if err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
		return
	}
	c.JSON(http.StatusOK, service.GetCommunityByID(id))
}
