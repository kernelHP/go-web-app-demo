package router

import (
	"fmt"
	"go-web-app-demo/controller/middleware"
	"go-web-app-demo/pkg/validator"
	"net"
	"net/http"
	"net/http/httputil"
	"os"
	"runtime/debug"
	"strings"
	"time"

	"github.com/gin-contrib/pprof"
	"github.com/gin-gonic/gin"
	gs "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"go.uber.org/zap"
)

type Handler struct {
	Method       string
	RelativePath string
	HandleFunc   gin.HandlerFunc
	NeedAuth     bool
}

var handlers []*Handler

func AddHandler(handler *Handler) {
	handlers = append(handlers, handler)
}

var Router *gin.Engine

func SetUp() *gin.Engine {

	if err := validator.InitTrans("zh"); err != nil {
		fmt.Printf("Init Validator Trans failed , err:%v\n", err)
		return nil
	}

	Router = gin.New()
	Router.Use(GinLogger(zap.L()), GinRecovery(zap.L(), true))

	// 限流
	Router.Use(middleware.RateLimitMiddleware(5*time.Millisecond, 1000))

	// swagger api doc
	Router.GET("/swagger/*any", gs.WrapHandler(swaggerFiles.Handler))

	for _, h := range handlers {

		if h.NeedAuth {
			Router.Use(middleware.JWTAuthMiddleware)
		}

		switch strings.ToUpper(h.Method) {
		case http.MethodGet:
			Router.GET(h.RelativePath, h.HandleFunc)
		case http.MethodPost:
			Router.POST(h.RelativePath, h.HandleFunc)
		case http.MethodPut:
			Router.PUT(h.RelativePath, h.HandleFunc)
		case http.MethodDelete:
			Router.DELETE(h.RelativePath, h.HandleFunc)
		default:
			fmt.Printf("Currently unsupported HTTP method:%s , relativePath:%s \n", h.Method, h.RelativePath)
		}
	}

	//  go tool pprof http://127.0.0.1:8081/debug/pprof/profile
	pprof.Register(Router)

	return Router
}

func GinLogger(logger *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		path := c.Request.URL.Path
		query := c.Request.URL.RawQuery
		c.Next()

		cost := time.Since(start)
		logger.Info(path,
			zap.Int("status", c.Writer.Status()),
			zap.String("method", c.Request.Method),
			zap.String("path", path),
			zap.String("query", query),
			zap.String("ip", c.ClientIP()),
			zap.String("user-agent", c.Request.UserAgent()),
			zap.String("errors", c.Errors.ByType(gin.ErrorTypePrivate).String()),
			zap.Duration("cost", cost),
		)
	}
}

// GinRecovery recover掉项目可能出现的panic
func GinRecovery(logger *zap.Logger, stack bool) gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				// Check for a broken connection, as it is not really a
				// condition that warrants a panic stack trace.
				var brokenPipe bool
				if ne, ok := err.(*net.OpError); ok {
					if se, ok := ne.Err.(*os.SyscallError); ok {
						if strings.Contains(strings.ToLower(se.Error()), "broken pipe") || strings.Contains(strings.ToLower(se.Error()), "connection reset by peer") {
							brokenPipe = true
						}
					}
				}

				httpRequest, _ := httputil.DumpRequest(c.Request, false)
				if brokenPipe {
					logger.Error(c.Request.URL.Path,
						zap.Any("error", err),
						zap.String("request", string(httpRequest)),
					)
					// If the connection is dead, we can't write a status to it.
					c.Error(err.(error)) // nolint: errcheck
					c.Abort()
					return
				}

				if stack {
					logger.Error("[Recovery from panic]",
						zap.Any("error", err),
						zap.String("request", string(httpRequest)),
						zap.String("stack", string(debug.Stack())),
					)
				} else {
					logger.Error("[Recovery from panic]",
						zap.Any("error", err),
						zap.String("request", string(httpRequest)),
					)
				}
				c.AbortWithStatus(http.StatusInternalServerError)
			}
		}()
		c.Next()
	}
}
