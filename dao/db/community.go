package db

import (
	"database/sql"
	"errors"
	"time"
)

type Community struct {
	ID            uint64    `json:"id,string" db:"id"`
	CommunityID   uint64    `json:"communityID,string" db:"community_id"`
	CommunityName string    `json:"communityName" db:"community_name"`
	Intro         string    `json:"intro" db:"intro"`
	CreateTime    time.Time `json:"createTime" db:"create_time"`
	UpdateTime    time.Time `json:"updateTime" db:"update_time"`
}

func ListCommunity() (communities []*Community, err error) {

	err = DB.Select(&communities, "select * from `community`")

	if errors.Is(err, sql.ErrNoRows) {
		communities = []*Community{}
		err = nil
	}
	return
}

func GetCommunityByID(id uint64) (c *Community, err error) {

	c = new(Community)
	err = DB.Get(c, "select * from `community` where community_id = ?", id)

	if errors.Is(err, sql.ErrNoRows) {
		err = nil
		return
	}
	return
}
