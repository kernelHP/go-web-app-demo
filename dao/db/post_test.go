package db

import (
	"go-web-app-demo/model"
	"go-web-app-demo/pkg/snowflake"
	"go-web-app-demo/settings"
	"go.uber.org/zap"
	"testing"
)

func init() {
	settings.Init("../../config.yaml")
	Init(settings.Conf.DBConfig)
	if err := snowflake.InitSnowFlake(settings.Conf.StartTime, settings.Conf.MachineID); err != nil {
		zap.L().Error("SnowFlake init error ", zap.Error(err))
		return
	}
}

func TestNewPost(t *testing.T) {

	req := &model.CreatePostRequest{
		CommunityID: 1,
		AuthorID:    1,
		Status:      1,
		Title:       "test",
		Content:     "just for test",
	}

	id, err := NewPost(req)

	if err != nil {
		t.Fatalf("NewPost test failed,err%v\n", err)
	}
	t.Logf("NewPost test succeed %v", id)
}
