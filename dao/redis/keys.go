package redis

const (
	KeyPostTimeZSet   = "go-web-app-demo:post:time"
	KeyPostScoreZSet  = "go-web-app-demo:post:score"
	KeyPostVotedPX    = "go-web-app-demo:post:voted:"
	KeyCommunitySetPX = "go-web-app-demo:community:"
)
