package model

type CreatePostRequest struct {
	CommunityID uint64 `json:"communityID" binding:"required"`
	AuthorID    uint64 `json:"authorID"`
	Status      int16  `json:"status" binding:"required"`
	Title       string `json:"title" binding:"required"`
	Content     string `json:"content" binding:"required"`
}
