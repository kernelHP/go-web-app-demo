FROM golang:alpine AS builder

ENV GO111MODULE=on \
    CGO_ENABLE=0 \
    GOPROXY=https://goproxy.cn,direct \
    GOOS=linux \
    GOARCH=amd64

WORKDIR /build

COPY . .

RUN go build -o go-web-app-demo .

COPY config.yaml /

FROM scratch

COPY --from=builder /build/go-web-app-demo /

EXPOSE 8888

ENTRYPOINT ["/go-web-app-demo" , "/config.yaml"]


