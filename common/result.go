package common

import "strings"

const (
	Succeed string = "succeed"
	Failed  string = "failed"
)

type Result struct {
	Code    string      `json:"code"`
	Msg     interface{} `json:"msg"`
	Payload interface{} `json:"payload"`
}

func (r *Result) IsSucceed() bool {
	return strings.EqualFold(r.Code, Succeed)
}

func NewSucceed(p interface{}) *Result {
	return NewResult(Succeed, "Request Succeed", p)
}

func NewFailed(msg interface{}) *Result {
	return NewResult(Failed, msg, nil)
}

func NewResult(code string, msg interface{}, p interface{}) *Result {
	return &Result{
		Code:    code,
		Msg:     msg,
		Payload: p,
	}
}
