package logger

import (
	"github.com/natefinch/lumberjack"
	"go-web-app-demo/settings"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
)

func Init(cfg *settings.LogConfig) {

	// 切割日志
	ll := &lumberjack.Logger{
		Filename:   cfg.Path,
		MaxSize:    int(cfg.MaxSize),
		MaxBackups: int(cfg.MaxBackups),
		MaxAge:     int(cfg.MaxAge),
		Compress:   cfg.Compress,
	}

	writeSyncer := zapcore.NewMultiWriteSyncer(zapcore.AddSync(ll), os.Stdout)
	conf := zap.NewProductionEncoderConfig()
	conf.EncodeTime = zapcore.ISO8601TimeEncoder
	conf.EncodeLevel = zapcore.CapitalLevelEncoder

	encoder := zapcore.NewConsoleEncoder(conf)

	level, _ := zapcore.ParseLevel(cfg.Level)
	core := zapcore.NewCore(encoder, writeSyncer, level)
	logger := zap.New(core, zap.AddCaller())
	zap.ReplaceGlobals(logger)
}
