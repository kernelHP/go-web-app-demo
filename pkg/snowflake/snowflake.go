package snowflake

import (
	"github.com/bwmarrin/snowflake"
	"github.com/sony/sonyflake"
	"time"
)

var node *snowflake.Node

func Init(startTime string, machineId int64) (err error) {

	var st time.Time
	st, err = time.Parse("2006-01-02", startTime)

	if err != nil {
		return
	}

	snowflake.Epoch = st.UnixNano() / 1000000
	node, err = snowflake.NewNode(machineId)
	return
}

func InitSnowFlake(startTime string, machineId int64) (err error) {
	var st time.Time
	st, err = time.Parse("2006-01-02", startTime)

	if err != nil {
		return
	}

	snowflake.Epoch = st.UnixNano() / 1000000
	node, err = snowflake.NewNode(machineId)
	return
}

func NextID() int64 {
	return node.Generate().Int64()
}

var sf *sonyflake.Sonyflake

func InitSonyFlake(startTime string, machineId int64) (err error) {

	var st time.Time
	st, err = time.Parse("2006-01-02", startTime)

	if err != nil {
		return
	}
	s := sonyflake.Settings{
		StartTime: st,
		MachineID: func() (uint16, error) {
			return uint16(machineId), nil
		},
		CheckMachineID: nil,
	}

	sf = sonyflake.NewSonyflake(s)

	return nil
}

func SonyNextID() (uint64, error) {
	return sf.NextID()
}
